//4. Find the sum of all salaries.

function totalSalaries(personData){
    if(Array.isArray(personData)){
        let totalSalary = personData.reduce((prevSalary,currSalary) => prevSalary + currSalary.modifiedSalary,0);
        return totalSalary;
    }
    else{
        return 0;
    }
}

module.exports = totalSalaries;