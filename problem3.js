//3. Assume that each salary amount is a factor of 10000 and correct it but add it as a new key (corrected_salary or something)

function getModifiedSalary(personData){
    if(Array.isArray(personData)){
        let modifiedSalary = personData.map((person) => {
            person.modifiedSalary = person.salary*10000;
            return person;
        });
        return modifiedSalary;
    }
    else{
        return [];
    }
}

module.exports = getModifiedSalary;