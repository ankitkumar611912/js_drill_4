//1. Find all Web Developers. ( It could be Web Developer III or Web Developer II or anything else )


function getAllWebDeveloper(personData){
    if(Array.isArray(personData)){
        let allWebDeveloper = personData.filter((person) => person.job.includes('Web Developer'));
        return allWebDeveloper;
    }
    else{
        return [];
    }
}

module.exports = getAllWebDeveloper;