//2. Convert all the salary values into proper numbers instead of strings.

function getProperSalaryDetails(personData){
    if(Array.isArray(personData)){
        let properSalaryDetails = personData.map((person) => {
            person.salary= parseFloat(person.salary.replace('$',''));
            return person;
        });
        return properSalaryDetails;
    }
    else{
        return [];
    }
}

module.exports = getProperSalaryDetails;