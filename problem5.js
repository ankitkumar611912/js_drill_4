//5. Find the sum of all salaries based on country. ( Group it based on country and then find the sum ).

function getTotalSalaryByCountry(personData){
    if(Array.isArray(personData)){
        const totalSalaryByCountry = personData.reduce((previous,current) =>{
            const country = current.location;
            if(!previous[country]){
                previous[country] = 0;
            }
            previous[country] += current.modifiedSalary;
            return previous;
        },{});
        return totalSalaryByCountry;

    }else{
        return [];
    }
}

module.exports = getTotalSalaryByCountry;