const personData = require('../data');
const getProperSalaryDetails = require('../problem2');
const getModifiedSalary = require('../problem3');
const totalSalaries = require('../problem4');

const modifiedPersonData = getModifiedSalary(getProperSalaryDetails(personData));

console.log(totalSalaries(modifiedPersonData));