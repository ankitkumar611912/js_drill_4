const personData = require('../data');
const getProperSalaryDetails = require('../problem2');
const getModifiedSalary = require('../problem3');

const averageSalariesByCountry = require('../problem6');

const getModifiedPersonData = getModifiedSalary(getProperSalaryDetails(personData));

console.log(averageSalariesByCountry(getModifiedPersonData));