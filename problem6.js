//6. Find the average salary of based on country. ( Groupd it based on country and then find the average ).

function getAverageSalaryByCountry(personData){
    if(Array.isArray(personData)){
        const totalSalaryByCountry = personData.reduce((previous,current) =>{
            const country = current.location;
            if(!previous[country]){
                previous[country] = 0;
            }
            previous[country] += current.modifiedSalary;
            return previous;
        },{});
        const countryCount = personData.reduce((previous,current) =>{
            const country = current.location;
            if(!previous[country]){
                previous[country] = 0;
            }
            previous[country] += 1;
            return previous;
        },{});
        let averageSalariesByCountry = {};
        for(const key in totalSalaryByCountry){
            averageSalariesByCountry[key] = totalSalaryByCountry[key] / countryCount[key];
        }
        return averageSalariesByCountry;

    }else{
        return [];
    }
}

module.exports = getAverageSalaryByCountry;